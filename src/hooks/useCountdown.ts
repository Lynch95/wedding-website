import { useState, useEffect } from 'react';


const useCountdown = (targetDate: string) => {
  const calculateTimeLeft = () => {
    const difference = +new Date(targetDate) - +new Date();
    let daysLeft = 0;

    if (difference > 0) {
      daysLeft = Math.floor(difference / (1000 * 60 * 60 * 24));
    }

    return { days: daysLeft };
  };

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

  useEffect(() => {
    const timer = setTimeout(() => {
      setTimeLeft(calculateTimeLeft());
    }, 1000 * 60 * 60 * 24);

    return () => clearTimeout(timer);
  }, [targetDate]);

  return timeLeft;
};

export default useCountdown;
