
import Form from '../components/Form'
import {HStack, Text, VStack, Image } from '@chakra-ui/react'
import emailjs from 'emailjs-com';
import { FormSchema } from '../components/Form';
import contactflower1  from  '/src/images/contactflower.jpeg'


function Contact() {

  const handleFormSubmit = (data: FormSchema) => {
    emailjs.send('service_c3q87co', 'template_33a2tuo', data, '6Nc1n9ZCYwUNoKcYQ')
      .then((response) => {
        console.log('SUCCESS!', response.status, response.text);
      }, (error) => {
        console.log('FAILED...', error);
      });
  };

  return (
<>
<HStack mt={{ base: '8', lg:'20'}} mx='8' justify='center' mb={12}>
<Text fontSize={{ base: "xl", lg: "3xl" }} fontWeight="extrabold">Contact Us</Text>
</HStack>

      
        <VStack mb={2}>
        <Image h='50vh' mt={2} mb={8} src={contactflower1}></Image>
      <Form onSubmit={handleFormSubmit} />
      
      </VStack>
      
      
    </>
  )
}

export default Contact