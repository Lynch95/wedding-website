import { VStack } from "@chakra-ui/react";
import Countdown from "../components/Countdown";
import Gallery from "../components/GalleryComp";
import niallhina1 from '/src/images/niallhina1.jpg';
import niallhina2 from '/src/images/niallhina2.jpg';
import niallhina3 from '/src/images/niallhina3.jpg';
import flowerBackground from '/src/images/flower.webp';

function Home() {
  const homeImages = [niallhina1, niallhina2, niallhina3];

  return (
    <>
      <VStack spacing={{ base: '1', lg: '4' }}>
        <Gallery images={homeImages} />
        <Countdown 
          targetDate="07/20/2024"
          title="WE'RE GETTING MARRIED"
          names="Niall & Hina"
          date="20/07/2024"
          backgroundImage={flowerBackground}
        />
      </VStack>
    </>
  );
}

export default Home;
