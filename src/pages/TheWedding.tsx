import { Box, VStack, Text, Wrap, WrapItem} from "@chakra-ui/react";
import Cards from "../components/Cards";
import Taxis from "../components/Taxis";
import churchImage from '/src/images/church.jpg'; 
import gablesImage from '/src/images/gables.jpg'; 
import GoogleMap from "../components/GoogleMap";
import oakwellImage from '/src/images/oakwell.jpg'
import townImage from '/src/images/town.jpg'

function TheWedding() {
  
  return (
    <>
      <Box 
      mt={{ base: 8, lg: 20 }} mb={{ base: 8, lg: 10 }}>
        <VStack justify='center'>
          <Text fontSize={{ base: "xl", lg: "3xl" }} fontWeight="extrabold">What will happen...</Text>
        </VStack>
      </Box>

      <Wrap justify='center' spacing="5" px="5">
        <WrapItem>
          <Cards 
            cardImages={churchImage}  
            heading="St Patricks Church"
            textbody="The wedding will be held at St Patricks in Birstall."
            time='2pm'
          />
        </WrapItem>
        <WrapItem>
          <Cards
            cardImages={gablesImage}  
            heading="The Gables"
            textbody="The wedding celebrations will take place in 'The Gables', a large marquee will be located outside in the spacious garden."
            time="4pm"
          />
        </WrapItem>
        </Wrap>
      

      <Box 
      mt={{ base: 8, lg: 20 }} mb={{ base: 8, lg: 10 }}>
        <VStack justify='center'>
          <Text fontSize={{ base: "xl", lg: "3xl" }} fontWeight="extrabold">How to get there...</Text>
          <Text>(Click the markers for address)</Text>
        </VStack>
        
      </Box>

     <GoogleMap />

     <Wrap justify='center' spacing="5" px="5" mt={{ base: 8, lg: 12}} >
        <WrapItem>
<Taxis 
title="Oakwell Taxis" 
TaxiImage={oakwellImage}
 phone="01924 471111" 
 description="https://oakwellrex.taxi/" />

</WrapItem>

<WrapItem mb={2}>
<Taxis
title="Town Cars" 
TaxiImage={townImage}
 phone="01924 470101"
 description="" />


</WrapItem>
        </Wrap>
    </>
  );
}

export default TheWedding;
