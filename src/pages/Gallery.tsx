import { Box, Image, Text } from '@chakra-ui/react';
import animalsImage from '/src/images/animals.jpg'; 

function Gallery() {
  return (
    <Box  w={{ base: "100vw", lg: "50vw" }}
   textAlign="center" my="4" maxW="1920px"  mx="auto" overflow="hidden">
      
      <Text mt={{base: '2', lg: 8}} fontSize={{ base: "md", sm: "lg", md: "xl" }} mb="4">
        Check back after the wedding for all the pictures! In the meantime, enjoy the animals...
      </Text>
      <Image src={animalsImage} alt="Animals" maxWidth="100%" />
    </Box>
  );
}

export default Gallery;
