import { Box, VStack, Image, Text, Divider, Link, Button } from "@chakra-ui/react";
import hotelImage from '/src/images/Hotel.jpg';

function Hotels() {
  return (
    <>
      <Box
        mt={{ base: 4, lg: 12 }}
        w={{ base: "100vw", lg: "80vw" }}
        maxW="1920px"
        position="relative"
        mx="auto"
        overflow="hidden"
        
      >
        <VStack justify='center' spacing={4}>
          <Text fontSize={{ base: "xl", lg: "2xl" }} fontWeight="extrabold">
            Village Hotel Leeds South
          </Text>
          <Image borderRadius={10} src={hotelImage} alt="Hotel" />
        </VStack>
      </Box>

<VStack justify='center'>
      <Box
        mt={4}
        p={{ base: 4, lg: 8 }}
        bg="gray.100" 
        boxShadow="md"
        borderRadius="lg" 
        mx={{ base: 4, lg: 8 }}
        mb={4}
        maxW={{ xl: "6xl" }}
      >
        <VStack align="start" spacing={3}>
          <Text fontSize={{ base: "xl", lg: "2xl" }} fontWeight="bold" mb={2}>
            Details
          </Text>
          <Divider />
          <Text><strong>Number of rooms:</strong> 20 Double occupancy rooms</Text>
          <Text><strong>Date of Stay:</strong> 19.07.2023</Text>
          <Text><strong>Number of Nights:</strong> 2 nights</Text>
          <Text><strong>Rate per room per night (single occupancy):</strong> £110.00 single occupancy, per room, per night</Text>
          <Text><strong>Rate per room per night (double/twin occupancy):</strong> £120.00 double occupancy, per room, per night</Text>
          <Text><strong>Room release date:</strong> 07.06.2024</Text>
        </VStack>
        <Link href="https://reservations.travelclick.com/109114?groupID=4077050" isExternal>
            <Button colorScheme="pink" mt={6}>
              Book Now
            </Button>
          </Link>
      </Box>
      </VStack>
    </>
  );
}

export default Hotels;
