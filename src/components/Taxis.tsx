import { VStack, Box, Image, Text, useBreakpointValue, Stack, Icon, Link } from '@chakra-ui/react'
import { PhoneIcon} from '@chakra-ui/icons'


interface Props {
title: string
TaxiImage: string
description: string
phone: string
}


const Taxis = ({title, TaxiImage, description, phone}: Props) => {
    const cardWidth = useBreakpointValue({ base: '90vw', sm: '80vw', md: 'sm' });
    
  return (
    
        <Box maxW={cardWidth} borderWidth="1px" borderRadius="lg" borderColor='black' overflow="hidden">
        <VStack justify='center'>
          <Text fontSize={{ base: "xl", lg: "2xl" }}>{title}</Text>
        
          <Image  w="full"
        h={{ base: '150px', md: '200px', lg: '250px' }} 
         src={TaxiImage}></Image>
         </VStack>
         <Stack direction="row" align="center" spacing={2} mt={2} mx={2}>
          <Icon as={PhoneIcon} w={6} h={6} />
          <Text fontSize="lg" fontWeight="bold">
            {phone}
          </Text>
          
        </Stack>
        <Link href={description} isExternal mx={2}>{description}</Link>


         </Box>
       
  )
}

export default Taxis