// Countdown.tsx
import { Box, Text, Image, VStack, useBreakpointValue } from '@chakra-ui/react';


import useCountdown from '../hooks/useCountdown';


interface CountdownProps {
    targetDate: string
    title: string
    names: string
    date: string
    backgroundImage: string
  }

const Countdown = ({ targetDate, title, names, date, backgroundImage }: CountdownProps) => {

const timeLeft = useCountdown(targetDate)

const CountImageFit = useBreakpointValue({ base: "cover", xl: "contain" }) 

  return (
    <Box position="relative" w="full" h="50vh" overflow="hidden" maxW='1920px'>
    <Image src={backgroundImage} alt="Event background" w="full" h="full" objectFit={CountImageFit as any} />
    <VStack
      position="absolute"
      top="50%"
      left="50%"
      transform="translate(-50%, -50%)"
      textAlign="center"
      color="white"
      spacing={4}
    >
      <Box bg="red.500" px={4} py={1} borderRadius="md" transform="translateY(-50%)" zIndex="banner">
        <Text fontSize={{ base: "md", md: "lg" }} fontWeight="bold">
          {timeLeft.days > 0 ? `ONLY ${timeLeft.days}` : "NO"} DAYS
        </Text>
      </Box>
      <Text fontSize="lg" fontWeight="semibold">
        {title}
      </Text>
      <Text fontSize={{ base: "1xl", lg: "6xl" }} fontWeight="extrabold" bg="blackAlpha.600" px={4} py={2} borderRadius="md">
        {names}
      </Text>
      <Text fontSize={{ base: "1xl" ,lg:"xl"}} fontWeight="bold" bg="blackAlpha.600" px={4} py={1} borderRadius="md">
        {date}
      </Text>
    </VStack>
  </Box>
  );
};

export default Countdown;
