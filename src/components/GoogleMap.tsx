import { useEffect, useState } from 'react';
import { APIProvider, Map, useMapsLibrary, useMap } from '@vis.gl/react-google-maps';
import { Box, Heading, Text, VStack, Button, List, ListItem, HStack } from '@chakra-ui/react';

export default function GoogleMap() {
    const position = { lat: 53.73208613114801, lng: -1.6654568530634992 };

    return (
        <HStack justify='center' mx={4} mb={4}>
        <Box 
        
        h={{ base: "50vh", lg: '30vh'}} 
        w={{base: "100vw", lg: '30vw'}}>
            <APIProvider apiKey={'AIzaSyCGGKFCSOD7EYCqW13nanvSowUOZTtc7CU'}>
                <Map zoom={9} center={position} mapId={'1ec09612a95bae6e'} fullscreenControl={false}>
                    <Directions />     
                </Map>
            </APIProvider>
        </Box>
        </HStack>
    );
}

function Directions() {
    const map = useMap() as google.maps.Map;
    const routesLibrary = useMapsLibrary('routes') as typeof google.maps;
    const [directionsService, setDirectionsService] = useState<google.maps.DirectionsService | null>(null);
    const [directionRenderer, setDirectionRenderer] = useState<google.maps.DirectionsRenderer | null>(null);
    const [routes, setRoutes] = useState<google.maps.DirectionsRoute[]>([]);
    const [routeIndex, setRouteIndex] = useState<number>(0);
    const selected = routes[routeIndex];
    const leg = selected?.legs[0];

    useEffect(() => {
        if (!routesLibrary || !map) return;
        setDirectionsService(new google.maps.DirectionsService());
        setDirectionRenderer(new google.maps.DirectionsRenderer({ map }));
    }, [routesLibrary, map]);

    useEffect(() => {
        if (!directionsService || !directionRenderer) return;

        directionsService.route({
            origin: 'Low Ln, Birstall, Batley WF17 9HD',
            destination: 'WF17 0AR, Upper Batley Ln 238, Batley',
            travelMode: google.maps.TravelMode.DRIVING,
            provideRouteAlternatives: true
        }).then(response => {
            directionRenderer.setDirections(response);
            setRoutes(response.routes);
        });
    }, [directionsService, directionRenderer]);

    useEffect(() => {
        if (!directionRenderer) return;
        directionRenderer.setRouteIndex(routeIndex);
    }, [routeIndex, directionRenderer]);

    if (!leg) return null;

    return (
        <VStack className="directions" spacing={4} p={4} >
            <Heading as="h2" size="lg">{selected.summary}</Heading>
            <Text fontSize="md">{leg.start_address.split(',')[0]} to {leg.end_address.split(',')[0]}</Text>
            <Text>Distance: {leg.distance?.text}</Text>
            <Text>Duration: {leg.duration?.text}</Text>

            <Heading as="h2" size="md">Other Routes</Heading>
            <List>
                {routes.map((route, index) => (
                    <ListItem key={index}>
                        <Button onClick={() => setRouteIndex(index)}>{route.summary}</Button>
                    </ListItem>
                ))}
            </List>
        </VStack>
    );
}
