import { ChevronLeftIcon, ChevronRightIcon } from '@chakra-ui/icons';
import { Box, Image, IconButton, useBreakpointValue, HStack } from '@chakra-ui/react';
import { useState } from 'react';

interface Props {
  images: string[];
}

const Gallery = ({ images }: Props) => {
  const [currentImage, setCurrentImage] = useState(0);

  const nextImage = () => {
    setCurrentImage((prev) => (prev + 1) % images.length);
  };

  const prevImage = () => {
    setCurrentImage((prev) => (prev - 1 + images.length) % images.length);
  };


  const objectFitValue = useBreakpointValue({ base: "cover", xl: "contain" }) || "cover";


  return (
    <Box
    mt={{base: 0, lg: 4}}
      w={{ base: "100vw", lg: "80vw" }}
      h={{ base: "50vh", lg: "80vh" }}
      maxW="1920px" 
      position="relative"
      mx="auto"
      overflow="hidden"
    >
      <Box
        position="absolute"
        top="50%"
        left="50%"
        transform="translate(-50%, -50%)"
        w="full"
        h="full"
      >
        <Image
          src={images[currentImage]}
          w="full"
          h="full"
          objectFit={objectFitValue as any}
         m={objectFitValue === 'contain' ? 'auto' : '0'}
          position={objectFitValue === 'contain' ? 'absolute' : 'static'}
          top={objectFitValue === 'contain' ? '50%' : 'none'}
          left={objectFitValue === 'contain' ? '50%' : 'none'}
          transform={objectFitValue === 'contain' ? 'translate(-50%, -50%)' : 'none'}
        />
      </Box>
      <HStack position="absolute" bottom="4" left="50%" transform="translateX(-50%)" spacing={4}>
        <IconButton
          borderRadius='50%'
          bg='pink'
          aria-label="Previous image"
          icon={<ChevronLeftIcon />}
          onClick={prevImage}
        />
        <IconButton
          borderRadius='50%'
          bg='pink'
          aria-label="Next image"
          icon={<ChevronRightIcon />}
          onClick={nextImage}
        />
      </HStack>
    </Box>
  );
};

export default Gallery;
