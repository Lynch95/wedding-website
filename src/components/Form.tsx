import { Button, Container, FormControl, FormErrorMessage, FormLabel, Input, VStack } from '@chakra-ui/react'
import { zodResolver } from '@hookform/resolvers/zod'
import { useForm } from 'react-hook-form'
import {z} from 'zod'


const schema = z.object({
  name: z.string().min(3, { message: "Name must be at least 3 characters" }).max(20, { message: "Name must be no longer than 20 characters" }),
  email: z.string().email({ message: "Invalid email address" }),
  message: z.string().min(1, { message: "Message is required" }),
})

export type FormSchema = z.infer<typeof schema>

interface Props {
  onSubmit: (data: FormSchema) => void
}

const Form = ({onSubmit}: Props) => {

const {handleSubmit, register, formState: {errors}, reset } = useForm<FormSchema>({resolver: zodResolver(schema)})

const onFormSubmit = (data: FormSchema) => {
  onSubmit(data);
  reset();
}

  return (
    <>
    
    <Container>
    <VStack spacing={4} as="form" onSubmit={handleSubmit(onFormSubmit)}
    mx={{ base: 2, sm: 4, md: 6, lg: 8 }}>
    <FormControl isInvalid={!!errors.name}>
      <FormLabel >Name</FormLabel>
      <Input {...register('name')}></Input>
      <FormErrorMessage>{errors.name?.message}</FormErrorMessage>
    </FormControl>


<FormControl isInvalid={!!errors.email}>
  <FormLabel>Email</FormLabel>
  <Input type='email' {...register('email')}></Input>
  <FormErrorMessage>{errors.email?.message}</FormErrorMessage>
</FormControl>

<FormControl isInvalid={!!errors.message}>
  <FormLabel>Message</FormLabel>
  <Input {...register('message')}></Input>
  <FormErrorMessage>{errors.message?.message}</FormErrorMessage>
</FormControl>
<Button colorScheme="pink" type="submit" w={{ base: 'full', md: 'auto' }}>
      Submit
    </Button>
</VStack>
</Container>

</>
  )
}

export default Form
