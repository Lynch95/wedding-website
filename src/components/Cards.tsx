
import { Box, Image, Heading, Stack, Text, useBreakpointValue, Icon } from '@chakra-ui/react';
import { FiClock } from 'react-icons/fi';

interface Props {
  cardImages: string;
  heading: string;
  textbody: string;
  time: string; 
}

const Cards = ({ cardImages, heading, textbody, time }: Props) => {
  const cardWidth = useBreakpointValue({ base: '90vw', sm: '80vw', md: 'sm' });

  return (
    <Box maxW={cardWidth} borderWidth="1px" borderRadius="lg" overflow="hidden">
      <Image
        src={cardImages}
        alt="Image"
        borderRadius="lg"
      />
      <Stack mt={6} spacing={3} p={4}>
        <Heading size={useBreakpointValue({ base: 'sm', md: 'md' })}>{heading}</Heading>
        
        <Stack direction="row" align="center" spacing={2}>
          <Icon as={FiClock} w={6} h={6} />
          <Text fontSize="lg" fontWeight="bold">
            {time}
          </Text>
        </Stack>

        <Text fontSize={useBreakpointValue({ base: 'sm', md: 'md' })}>
          {textbody}
        </Text>
      </Stack>
    </Box>
  );
};

export default Cards;
