
import { Box, Link as ChakraLink, HStack } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";

const NavBar = () => {
  return (
    <Box bg='pink' w='100%' p={4} color='white'>
      <HStack spacing={{ base: 2, lg: 4}} as="nav" justify='center' >
        <ChakraLink as={RouterLink} to="/">Home</ChakraLink>
        <ChakraLink as={RouterLink} to="/TheWedding">Wedding</ChakraLink>
        <ChakraLink as={RouterLink} to="/Hotels">Hotels</ChakraLink>
        <ChakraLink as={RouterLink} to="/Gallery">Gallery</ChakraLink>
        <ChakraLink as={RouterLink} to="/Contact">Contact</ChakraLink>
        
      </HStack>
    </Box>
  );
}

export default NavBar;
