import { Box, VStack } from '@chakra-ui/react';
import NavBar from './components/NavBar';
import Home from './pages/Home';
import TheWedding from './pages/TheWedding'; 
import Contact from './pages/Contact';
import Gallery from './pages/Gallery';
import Footer from './components/Footer';
import Hotels from './pages/Hotels';
import { Route, Routes } from 'react-router-dom';


function App() {
  return (
    <VStack spacing={0} align="stretch" minHeight="100vh" backgroundColor="#F4F3F2">
      <NavBar />
      <Box as="main" flex="1">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/thewedding" element={<TheWedding />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/gallery" element={<Gallery />} />
          <Route path="/hotels" element={<Hotels />} />
        </Routes>
      </Box>
      <Footer footText="Contact the Groom" />
    </VStack>
  );
}

export default App;
